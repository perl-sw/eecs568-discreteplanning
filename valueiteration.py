import os

import numpy as np
import matplotlib.pyplot as plt

from grid import Grid

class Value_iteration (object):
    """
    *very* basic value iteration method, iterates over entire state space to
    update value function

    assumes policy is simply moving to the neighboring state with lower value
    (cost). note we don't care about start here... once we have a value
    function, we can find actions for *any* start state.
    """
    def __init__ (self, space, sgoal):
        self.space = space
        self.sgoal = sgoal

        self.states = self.space.list_all ()
        self.value = {s:np.inf for s in self.states}
        self.value[self.sgoal] = 0.

    def plan (self, iters=10, savefigs=False, rootpath='.'):
        """
        loop over states updating value function for iters number of iterations
        """
        for i in xrange (iters):
            if savefigs:
                self.savefig (os.path.join (rootpath, 'valueiteration%03d.png' % i))
            for s in self.states: # loop over all states
                # update value based on minimium new value
                alist = self.space.actions (s)
                for a in alist:
                    sn = self.space.apply_action (s, a)
                    v = self.value[sn] + self.space.action_cost ()
                    if v < self.value[s]:
                        self.value[s] = v

    def plot (self, ax):
        # plot state space
        self.space.plot (ax)

        # plot state values
        realvals = np.array ([v for v in self.value.itervalues () if v < np.inf])
        vmax = realvals.max ()
        
        cmap = plt.get_cmap ('YlOrRd')
        for s,v in self.value.iteritems ():
            if np.isinf (v): continue
            rgb = cmap (v/vmax)
            self.space.plot_state (ax, s, ec='none', fc=rgb)

        # plot goal
        self.space.plot_state (ax, self.sgoal, ec='none', fc='r')

        ax.axis ('equal')

    def savefig (self, path):
        fig = plt.figure (figsize=(10,10))
        ax = fig.add_subplot (111)
        self.plot (ax)
        ax.set_axis_off ()

        # hack to center grid environment...
        ax.axis ([-0.1,self.space.width+0.1,-0.1,self.space.length+0.1])

        fig.tight_layout ()
        fig.savefig (path, bbox_inches='tight')
        plt.close (fig)



if __name__ == '__main__':
    import sys

    print 'testing forward search on grid...'
    savefigs = False
    if len (sys.argv) > 1 and sys.argv[1].lower ()=='savefigs':
        savefigs = True

    # create grid environment
    grid = Grid (15, 15)

    obs0 = [(3,2), (3,3), (3,4), (3,5), (4,2), (5,2), (1,6), (2,6), (3,6)]
    obs1 = [(5,9), (5,8), (5,7)]
    obs2 = [(5,4), (6,4), (7,4), (8,4), (8,5), (9,5)]
    obs3 = [(9,9), (10,9), (11,9), (12,9), (13,9), (14,9)]
    grid.add_obstacles (obs0)
    grid.add_obstacles (obs1)
    grid.add_obstacles (obs2)
    grid.add_obstacles (obs3)

    # plan...
    sgoal = (14,13)

    print 'value iteration from goal', sgoal
    val = Value_iteration (grid, sgoal)
    if savefigs:
        os.mkdir ('valiteration')
        val.plan (50, savefigs=True, rootpath='valiteration')
    else:
        val.plan (50)
        fig = plt.figure ()
        ax = fig.add_subplot (111)
        val.plot (ax)
        plt.show ()

    sys.exit (0)
