import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

class Grid (object):
    """
    implements basic grid world state space. each cell is represented as a tuple
    containing the x,y coordinates of the cell.

    Parameters
    -----------
    length : number of vertical grid cells
    width : number of horizontal grid cells

    obstacles : list of occupied cells
    """
    def __init__ (self, length, width):
        self.length = length
        self.width = width

        self.obstacles = []

    def add_obstacles (self, olist):
        """
        mark cell(s) as obstacles, does not verify that indices are valid

        Parameters
        -----------
        olist: list of obstacles indices tuple of x,y coordinates
        """
        self.obstacles.extend (olist)

    def list_all (self):
        """
        returns list of all obstacle-free states in environment, used for value
        iteration
        """
        states =  [(i,j) for i in xrange (self.length) for j in xrange (self.width)
                if not (i,j) in self.obstacles]
        return states

    def actions (self, s):
        """
        returns valid actions from state s

        Parameters
        -----------
        s : state (tuple of x,y coordinates)

        Returns
        --------
        alist : list of valid actions (tuple of delta x, delta y coordinates)
        """
        if s in self.obstacles: return []

        alist = []
        if not s[0]<=0 and not (s[0]-1, s[1]) in self.obstacles:
            alist.append ((-1,0))
        if not s[1]>=(self.length-1) and not (s[0], s[1]+1) in self.obstacles:
            alist.append ((0,1))
        if not s[0]>=(self.width-1) and not (s[0]+1, s[1]) in self.obstacles:
            alist.append ((1,0))
        if not s[1]<=0 and not (s[0], s[1]-1) in self.obstacles:
            alist.append ((0,-1))
        return alist

    def apply_action (self, s, a):
        """
        apply action a to state s, assumes actions is valid

        Parameters
        -----------
        s : state (tuple of x,y coordinates)
        a : action (tuple of delta x, delta y coordinates)

        Returns
        --------
        sn : new state after applying action
        """
        return (s[0]+a[0], s[1]+a[1])

    def action_cost (self):
        """
        return cost to apply action

        should really be based on applying action a at state s, i.e., should
        take the action and state as input args. For example, consider an
        8-connected grid environment, perhaps diagonal actions have greater cost
        than up-down/left-right.
        """
        return 1

    def heuristic_distance (self, s0, s1):
        """
        implements heuristic distance (necessary for astar)

        Parameters
        -----------
        s0, s1 : states in space

        Returns
        --------
        distance : estimate of cost to get to s1 from s0 without obstacles
        """
        return abs (s0[0]-s1[0]) + abs (s0[1]-s1[1]) # manhattan distance

    def plot (self, ax):
        ax.vlines (np.arange (self.length + 1), 0, self.length, lw=4, color='0.7')
        ax.hlines (np.arange (self.width + 1), 0, self.width, lw=4, color='0.7')

        for o in self.obstacles:
            ax.add_artist (Rectangle (o, 1, 1, ec='none', fc='0.7'))

        ax.set_xticklabels ([])
        ax.set_yticklabels ([])

    def plot_state (self, ax, s, *args, **kwargs):
        ax.add_artist (Rectangle (s, 1, 1, *args, **kwargs))

    def plot_state_state (self, ax, s, sn, *args, **kwargs):
        ax.plot ([s[0]+0.5, sn[0]+0.5], [s[1]+0.5, sn[1]+0.5], *args, **kwargs)



if __name__ == '__main__':
    import sys
    print 'testing grid...'

    grid = Grid (10,10)

    olist = [(1,1), (1,2), (1,3), (1,4)]
    grid.add_obstacles (olist)
    print 'added obstacles at:', olist

    print 'actions from (0,0):', grid.actions ((0,0)) # bottom left corner
    print 'actions from (9,9):', grid.actions ((9,9)) # top right corner
    print 'actions from (7,3):', grid.actions ((7,3)) # middle of grid
    print 'actions from (2,3):', grid.actions ((2,3)) # obstacle

    sys.exit (0)
