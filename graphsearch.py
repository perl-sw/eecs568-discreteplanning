import os
from collections import deque
import heapq

import matplotlib.pyplot as plt

from grid import Grid


class Forward_search (object):
    """
    discrete planning template as outlined by LaValle (for single query
    planning)

    Parameters
    -----------
    space : abstract state/action space
    sstart : start state
    sgoal : goal state

    visited : list of visited states, used during planning
    searchtree : map from visited states to their preceeding states in the plan
    """
    def __init__ (self, space, sstart, sgoal):
        self.space = space
        self.sstart = sstart
        self.sgoal = sgoal

        self.visited = []
        self.searchtree = {} # keep track of actions (map from state to parent state)

    def plan (self, savefigs=False, rootpath='.'):
        """
        plan using skeleton forward graph search from LaValle.

        Returns
        --------
        bool if path found
        """
        self._push (self.sstart)

        iters, drawnew = 0, True
        while not self._empty ():
            if savefigs and drawnew:
                self.savefig (os.path.join (rootpath, 'forwardsearch%03d.png' % iters))

            s = self._pop ()
            if s == self.sgoal:
                return True

            alist = self.space.actions (s)
            drawnew = False
            for a in alist:
                sn = self.space.apply_action (s, a)
                if not sn in self.visited:
                    self.visited.append (sn)
                    self.searchtree[sn] = s # must happen before push (to compute cost to come)
                    self._push (sn)
                    drawnew = True
                else:
                    self._resolve (s, sn)
            iters += 1
        return False

    def _push (self, s):
        raise NotImplementedError ('_push () not implemented!')

    def _pop (self):
        raise NotImplementedError ('_pop () not implemented!')

    def _empty (self):
        raise NotImplementedError ('_empty () not implemented!')

    def _resolve (self, s, sn):
        raise NotImplementedError ('_resolve () not implemented!')

    def plot (self, ax):
        # plot state space
        self.space.plot (ax)

        # plot start/goal
        self.space.plot_state (ax, self.sstart, ec='none', fc='g')
        self.space.plot_state (ax, self.sgoal, ec='none', fc='r')

        # plot search tree
        for sn, s in self.searchtree.iteritems ():
            self.space.plot_state_state (ax, s, sn,
                    'k-o', ms=15, mec='k', mfc='w', mew=4, lw=4) 

        # plot final path
        if self.sgoal in self.searchtree:
            s = self.sgoal
            while s != self.sstart:
                sn = s
                s = self.searchtree[sn]
                self.space.plot_state_state (ax, s, sn,
                        'y-o', ms=17, mec='y', mfc='y', mew=4, lw=6) 
        ax.axis ('equal')
 
    def savefig (self, path):
        fig = plt.figure (figsize=(10,10))
        ax = fig.add_subplot (111)
        self.plot (ax)
        ax.set_axis_off ()

        # hack to center grid environment...
        ax.axis ([-0.1,self.space.width+0.1,-0.1,self.space.length+0.1])

        fig.tight_layout ()
        fig.savefig (path, bbox_inches='tight')
        plt.close (fig)


class Breadth_first (Forward_search):
    """
    queue is simple FIFO stack
    """
    def __init__ (self, space, sstart, sgoal):
        super (Breadth_first, self).__init__ (space, sstart, sgoal)
        self.queue = deque ()

    def _push (self, s):
        self.queue.append (s)

    def _pop (self):
        return self.queue.popleft ()

    def _empty (self):
        return False if self.queue else True

    def _resolve (self, s, sn):
        pass


class Depth_first (Forward_search):
    """
    queue is simple LIFO stack
    """
    def __init__ (self, space, sstart, sgoal):
        super (Depth_first, self).__init__ (space, sstart, sgoal)
        self.queue = deque ()

    def _push (self, s):
        self.queue.append (s)

    def _pop (self):
        return self.queue.pop ()

    def _empty (self):
        return False if self.queue else True

    def _resolve (self, s, sn):
        pass


class Priority_queue (object):
    """
    basic priority queue implemented with heapq
    """
    def __init__ (self):
        self.pq = []

    def push (self, x):
        heapq.heappush (self.pq, x)

    def pop (self):
        return heapq.heappop (self.pq)

    def empty (self):
        return False if self.pq else True


class Dijkstras (Forward_search):
    """
    queue prioritized on cost-to-come
    """
    class State (object):
        def __init__ (self, state, costtocome):
            self.state = state
            self.costtocome = costtocome

        def __cmp__ (self, s):
            return self.costtocome - s.costtocome

    def __init__ (self, space, sstart, sgoal):
        super (Dijkstras, self).__init__ (space, sstart, sgoal)
        self.queue = Priority_queue ()
        self.costtocome = {self.sstart:0} # map state to cost-to-come

    def _push (self, s):
        if s != self.sstart:
            self.costtocome[s] = self.costtocome[self.searchtree[s]] + self.space.action_cost ()
        self.queue.push (Dijkstras.State (s, self.costtocome[s]))

    def _pop (self):
        return self.queue.pop ().state

    def _empty (self):
        return self.queue.empty ()

    def _resolve (self, s, sn):
        if self.costtocome[sn] < self.costtocome[s] + self.space.action_cost ():
            return
        self.searchtree[sn] = s
        self.costtocome[sn] = self.costtocome[s] + self.space.action_cost ()
        # TODO: update priority queue...


class Astar (Forward_search):
    """
    queue prioritized on cost-to-come plus heuristic cost-to-go
    """
    class State (object):
        def __init__ (self, state, costtocome, costtogo):
            self.state = state
            self.costtocome = costtocome
            self.costtogo = costtogo

        def __cmp__ (self, s):
            return (self.costtocome+self.costtogo) - (s.costtocome+s.costtogo)

    def __init__ (self, space, sstart, sgoal):
        super (Astar, self).__init__ (space, sstart, sgoal)
        self.queue = Priority_queue ()
        self.costtocome = {self.sstart:0} # map state to cost-to-come

    def _push (self, s):
        costtogo = self.space.heuristic_distance (s, self.sgoal)
        if s != self.sstart:
            self.costtocome[s] = self.costtocome[self.searchtree[s]] + self.space.action_cost ()
        self.queue.push (Astar.State (s, self.costtocome[s], costtogo))

    def _pop (self):
        return self.queue.pop ().state

    def _empty (self):
        return self.queue.empty ()

    def _resolve (self, s, sn):
        if self.costtocome[sn] < self.costtocome[s] + self.space.action_cost ():
            return
        self.searchtree[sn] = s
        self.costtocome[sn] = self.costtocome[s] + self.space.action_cost ()
        # TODO: update priority queue...




if __name__ == '__main__':
    import sys

    print 'testing forward search on grid...'
    savefigs = False
    if len (sys.argv) > 1 and sys.argv[1].lower ()=='savefigs':
        savefigs = True


    # create grid environment
    grid = Grid (15, 15)

    obs0 = [(3,2), (3,3), (3,4), (3,5), (4,2), (5,2), (1,6), (2,6), (3,6)]
    obs1 = [(5,9), (5,8), (5,7)]
    obs2 = [(5,4), (6,4), (7,4), (8,4), (8,5), (9,5)]
    obs3 = [(9,9), (10,9), (11,9), (12,9), (13,9), (14,9)]
    grid.add_obstacles (obs0)
    grid.add_obstacles (obs1)
    grid.add_obstacles (obs2)
    grid.add_obstacles (obs3)

    
    # plan...
    sstart = (1,1)
    sgoal = (14,13)


    print 'planning bfs from start', sstart, 'to goal', sgoal
    bfs = Breadth_first (grid, sstart, sgoal)
    if savefigs:
        os.mkdir ('bfs')
        bfs.plan (savefigs=True, rootpath='bfs')
    else:
        bfs.plan ()
        fig = plt.figure ()
        ax = fig.add_subplot (111)
        bfs.plot (ax)
        plt.show ()


    print 'planning dfs from start', sstart, 'to goal', sgoal
    dfs = Depth_first (grid, sstart, sgoal)
    if savefigs:
        os.mkdir ('dfs')
        dfs.plan (savefigs=True, rootpath='dfs')
    else:
        dfs.plan ()
        fig = plt.figure ()
        ax = fig.add_subplot (111)
        dfs.plot (ax)
        plt.show ()


    print 'planning dijkstras from start', sstart, 'to goal', sgoal
    dikstras = Dijkstras (grid, sstart, sgoal)
    if savefigs:
        os.mkdir ('dijkstras')
        dikstras.plan (savefigs=True, rootpath='dijkstras')
    else:
        dikstras.plan ()
        fig = plt.figure ()
        ax = fig.add_subplot (111)
        dikstras.plot (ax)
        plt.show ()


    print 'planning astar from start', sstart, 'to goal', sgoal
    astar = Astar (grid, sstart, sgoal)
    if savefigs:
        os.mkdir ('astar')
        astar.plan (savefigs=True, rootpath='astar')
    else:
        astar.plan ()
        fig = plt.figure ()
        ax = fig.add_subplot (111)
        astar.plot (ax)
        plt.show ()


    sys.exit (0)
