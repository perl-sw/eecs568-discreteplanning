###EECS 568 discrete planning

Playground for discrete planning (search plus value iteration) on a simplified
grid world... This demo is available as is, for support click
[here](http://lmgtfy.com/).

Requires:
    
* python (>= 2.6 should work, not python 3)
* numpy
* matplotlib

The grid environment is implemented in `grid.py`. States are cells within the
grid, actions move from one cell to a neighbor where neighbors are defined by a
four-connected network. The planning methods are implemented for an abstract
`space`, but have only been tested with the grid environment. Try creating a new
environment, perhaps a grid with eight-connected action space.


---------------
Forward search

To run:

    $ python graphsearch.py

What methods seem to work best? Which produce optimal paths? How does obstacle
configuration affect the performance of each planner?


----------------
Value iteration

To run:

    $ python valueiteration.py

How many iterations does it take for value iteration to converge? Try playing
around with difference grid dimensions and obstacle configurations.

How could you extend this method such that the motion model is stochastic? i.e.,
if an action is applied there is a distribution over next states... How does
convergence change in this scenario?


------------
Jeff Walls <jmwalls@umich.edu>
